import React from "react";
class App extends React.Component {
  state = {
    a: 21,
    b: 23,
    result: 0,
    meter: 0,
    step: 1,
  };
  Add = () => this.setState((state) => (state.result += state.a + state.b));
  Minus = () => this.setState((state) => (state.result = state.a - state.b));
  Multiple = () => this.setState((state) => (state.result = state.a * state.b));
  Divide = () => this.setState((state) => (state.result = state.a / state.b));
  walk = () => this.setState((state) => (state.meter += state.step));
  step = () => this.setState((state) => (state.step += 1));
  render() {
    const { a, b, result, meter, step } = this.state;
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <h1>a = {a}</h1>
        <h1>b = {b}</h1>
        <h1>Result = {result}</h1>
        <main style={{ display: "flex", gap: "15px" }}>
          <button
            onClick={this.Add}
            style={{
              background: "transparent",
              padding: "10px 18px",
              borderRadius: "10px",
            }}
          >
            a + b
          </button>
          <button
            onClick={this.Minus}
            style={{
              background: "transparent",
              padding: "10px 18px",
              borderRadius: "10px",
            }}
          >
            a - b
          </button>
          <button
            onClick={this.Multiple}
            style={{
              background: "transparent",
              padding: "10px 18px",
              borderRadius: "10px",
            }}
          >
            a * b
          </button>
          <button
            onClick={this.Divide}
            style={{
              background: "transparent",
              padding: "10px 18px",
              borderRadius: "10px",
            }}
          >
            a / b
          </button>
        </main>

        <main
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <h1>{meter} metr</h1>
          <button
            onClick={this.walk}
            style={{
              background: "transparent",
              padding: "10px 18px",
              borderRadius: "10px",
            }}
          >
            Yurish
          </button>
          <h1>Qadam kattaligi: {step} metr</h1>
          <button
            onClick={this.step}
            style={{
              background: "transparent",
              padding: "10px 18px",
              borderRadius: "10px",
            }}
          >
            Qadam kengaytirish
          </button>
        </main>
      </div>
    );
  }
}
export default App;
